using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using functionapp_automation_workplace.Handlers;
using Models;
using System.Net.Http;
using System.Net.Http.Headers;

//var zoranUserId = "100012820865058"; // username= "zoran.despotovski@deptagency.com"
namespace functionapp_automation_workplace
{
    public static class GetUsersFunction
    {
        [FunctionName("GetUsersByIdFunction")]
        public static async Task<IActionResult> RunGetUsersByIdFunction(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            long userId = 0;
            string userIdString = req.Query["userId"];
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            userIdString = userIdString ?? data?.userId;
            long.TryParse(userIdString, out userId);

            var wUser = await WorkplaceHandler.GetUser(log, userId);

            return wUser != null
                ? (ActionResult)new OkObjectResult(wUser)
                : new BadRequestObjectResult("Please pass a 'userId' on the query string or in the request body");
        }
    }
}
