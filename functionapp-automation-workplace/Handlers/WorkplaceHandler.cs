﻿using Microsoft.Extensions.Logging;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace functionapp_automation_workplace.Handlers
{
    public static class WorkplaceHandler
    {
        private static readonly string _workplaceBaseUrl = Environment.GetEnvironmentVariable("FacebookWorkplaceServerUrl");
        private static readonly string _workplaceToken = Environment.GetEnvironmentVariable("FacebookWorkplaceBearerToken");

        private static HttpClientHandler _httpClientHandler = new HttpClientHandler();
        private static HttpClient _httpClient = new HttpClient(_httpClientHandler);

        static WorkplaceHandler()
        {
            InitializeHttpClient();
        }
        private static void InitializeHttpClient()
        {
            _httpClient.BaseAddress = new Uri(_workplaceBaseUrl);
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _workplaceToken);
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Add("User-Agent", "AzureFunction");
        }

        public static async Task<FacebookUser> GetUser(ILogger log, long userId)
        {
            log.LogInformation($"{nameof(GetUser)}?/{userId} start.");

            var urlReq = $"Users/{userId}";
            var response = await _httpClient.GetAsync(urlReq);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseContentString = await response.Content.ReadAsStringAsync();
                var user = JsonConvert.DeserializeObject<FacebookUser>(responseContentString, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                return user;
            }
            else
            {
                log.LogError($@"Failed to return FacebookWorkplaceUserr Id[{userId}]. Response status code: [{response.StatusCode}], message:[{response.Content}]");
                return null;
            }
        }
        public static async Task<FacebookUser> GetUser(ILogger log, string userName)
        {
            log.LogInformation($"{nameof(GetUser)}?filter=username eq {nameof(userName)} start.");

            var urlReq = $"Users?filter=" + HttpUtility.UrlEncode($"userName eq \"{userName}\"");
            var response = await _httpClient.GetAsync(urlReq);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseContentString = await response.Content.ReadAsStringAsync();
                var usersObj = JsonConvert.DeserializeObject<FacebookUsersObj>(responseContentString, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                return usersObj.Resources.FirstOrDefault(x => x.UserName == userName);
            }
            else
            {
                log.LogError($@"Failed to return FacebookWorkplaceUser for username =[{userName}]. Response status code: [{response.StatusCode}], message:[{response.Content}]");
                return null;
            }
        }
        public static async Task<FacebookUser> CreateUser(ILogger log, FacebookUser createUser)
        {
            log.LogInformation($"{nameof(CreateUser)} start.");

            var stringContent = new StringContent(JsonConvert.SerializeObject(createUser));
            var response = await _httpClient.PostAsync("Users/", stringContent);
            if (response.StatusCode == System.Net.HttpStatusCode.Created)
            {
                var responseContentString = await response.Content.ReadAsStringAsync();
                var user = JsonConvert.DeserializeObject<FacebookUser>(responseContentString, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                return user;
            }
            else
            {
                log.LogError($@"Failed to create FacebookWorkplaceUser employeeId[{createUser.UserName ?? ""}]
                                    Response status code: [{response.StatusCode}], message:[{response.Content}]");
                return null;
            }
        }
        public static async Task<FacebookUser> UpdateUser(ILogger log, FacebookUser updateUser, long userId)
        {
            log.LogInformation($"{nameof(UpdateUser)} start.");

            var stringContent = new StringContent(JsonConvert.SerializeObject(updateUser));
            var response = await _httpClient.PutAsync($"Users/{userId}", stringContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseContentString = await response.Content.ReadAsStringAsync();
                var user = JsonConvert.DeserializeObject<FacebookUser>(responseContentString, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                return user;
            }
            else
            {
                log.LogError($@"Failed to update FacebookWorkplaceUser Id[{userId}]. Response status code: [{response.StatusCode}], message:[{response.Content}]");
                return null;
            }
        }
    }
}
