﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    public class Address
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "primary")]
        public bool Primary { get; set; }

        [DataMember(Name = "formatted")]
        public string Formatted { get; set; }

        [DataMember(Name = "locality")]
        public string Locality { get; set; }

    }
}