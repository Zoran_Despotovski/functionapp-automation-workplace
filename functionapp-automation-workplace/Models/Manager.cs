﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    public class Manager
    {
        [DataMember(Name = "managerId")]
        public Int64 ManagerId { get; set; }
    }
}
