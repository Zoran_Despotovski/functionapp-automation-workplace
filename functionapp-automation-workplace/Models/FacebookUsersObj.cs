﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    public class FacebookUsersObj
    {
        [DataMember(Name = "totalResults", EmitDefaultValue = true)]
        public int totalResults { get; set; }
        [DataMember(Name = "Resources", EmitDefaultValue = false)]
        public List<FacebookUser> Resources { get; set; }
    }
}
