﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract(Name = "urn:scim:schemas:extension:enterprise:1.0")]
    public class EnterpriseSchema
    {
        [DataMember(Name = "division")]
        public string Division { get; set; }

        [DataMember(Name = "department")]
        public string Department { get; set; }

        [DataMember(Name = "manager")]
        public Manager Manager { get; set; }
    }
}
