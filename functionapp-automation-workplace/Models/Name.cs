﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    public class Name
    {
        [DataMember(Name = "givenName")]
        public string GivenName { get; set; }

        [DataMember(Name = "familyName")]
        public string FamilyName { get; set; }

        [DataMember(Name = "formatted", IsRequired = true)]
        public string Formatted { get; set; }
    }
}
