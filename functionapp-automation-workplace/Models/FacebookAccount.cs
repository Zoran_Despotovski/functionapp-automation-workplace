﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class FacebookAccount
    {
        public string ServerUrl
        {
            get; set;
        }
        public string Token
        {
            get; set;
        }
    }

}
