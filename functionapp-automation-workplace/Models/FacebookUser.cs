﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    public class FacebookUser
    {
        public FacebookUser()
        {

        }

        [DataMember(Name = "schemas", IsRequired = false, EmitDefaultValue = false)]
        public List<string> Schemas { get; set; }

        [DataMember(Name = "externalId", EmitDefaultValue = true)]
        public string ExternalId { get; set; }

        [DataMember(Name = "id", EmitDefaultValue = true)]
        public Int64 Id { get; set; }

        [DataMember(Name = "userName", EmitDefaultValue = true)]
        public string UserName { get; set; }

        [DataMember(Name = "name", EmitDefaultValue = true)]
        public Name Name { get; set; }

        [DataMember(Name = "title", EmitDefaultValue = true)]
        public string Title { get; set; }

        [DataMember(Name = "displayName", EmitDefaultValue = true)]
        public string DisplayName { get; set; }

        [DataMember(Name = "userType", EmitDefaultValue = true)]
        public string UserType { get; set; }

        [DataMember(Name = "timezone", EmitDefaultValue = true)]
        public string Timezone { get; set; }

        [DataMember(Name = "locale", EmitDefaultValue = true)]
        public string Locale { get; set; }

        [DataMember(Name = "active", IsRequired = true)]
        public bool Active { get; set; }

        [DataMember(Name = "addresses", IsRequired = false, EmitDefaultValue = true)]
        public List<Address> Addresses { get; set; }

        [DataMember(Name = "emails", IsRequired = false, EmitDefaultValue = true)]
        public List<Email> Emails { get; set; }

        [DataMember(Name = "phoneNumbers", IsRequired = false, EmitDefaultValue = true)]
        public List<PhoneNumber> PhoneNumbers { get; set; }

        [DataMember(Name = "urn:scim:schemas:extension:enterprise:1.0", EmitDefaultValue = true)]
        public EnterpriseSchema EnterpriseSchema { get; set; }

        [DataMember(Name = "urn:scim:schemas:extension:facebook:starttermdates:1.0", EmitDefaultValue = true)]
        public StartTermDatesSchema StartTermDatesSchema { get; set; }

        [DataMember(Name = "urn:scim:schemas:extension:facebook:accountstatusdetails:1.0", EmitDefaultValue = true)]
        public AccountStatusDetailsSchema AccountStatusDetailsSchema { get; set; }

        [DataMember(Name = "urn:scim:schemas:extension:facebook:auth_method:1.0", EmitDefaultValue = true)]
        public AuthMethodSchema AuthMethodSchema { get; set; }
        
    }
}
