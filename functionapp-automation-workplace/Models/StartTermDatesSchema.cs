﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract(Name = "urn:scim:schemas:extension:facebook:starttermdates:1.0")]
    public class StartTermDatesSchema
    {
        [DataMember(Name = "startDate")]
        public int StartDate { get; set; }

        [DataMember(Name = "termDate")]
        public int TermDate { get; set; }
    }
}
