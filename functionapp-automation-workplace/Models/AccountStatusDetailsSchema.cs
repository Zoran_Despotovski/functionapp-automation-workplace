﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    public class AccountStatusDetailsSchema
    {
        [DataMember(Name = "claimed")]
        public bool Claimed { get; set; }

        [DataMember(Name = "claimDate")]
        public Int32 ClaimDate { get; set; }

        [DataMember(Name = "invited")]
        public bool Invited { get; set; }

        [DataMember(Name = "inviteDate")]
        public Int32 InviteDate { get; set; }
    }
}
