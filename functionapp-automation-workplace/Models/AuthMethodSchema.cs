﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    public class AuthMethodSchema
    {
        [DataMember(Name = "auth_method")]
        public string AuthMethod { get; set; }
    }
}