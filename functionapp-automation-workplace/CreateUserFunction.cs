using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using functionapp_automation_workplace.Handlers;
using Models;
using System.Net.Http;
using System.Net.Http.Headers;

//var zoranUserId = "100012820865058"; // username= "zoran.despotovski@deptagency.com"
namespace functionapp_automation_workplace
{
    public static class CreateUserFunction
    {
        [FunctionName("CreateUserFunction")]
        public static async Task<IActionResult> RunCreateUserFunction(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            FacebookUser newUserData = JsonConvert.DeserializeObject<FacebookUser>(requestBody);

            var wUser = await WorkplaceHandler.CreateUser(log, newUserData);

            return wUser != null
                ? (ActionResult)new OkObjectResult(wUser)
                : new BadRequestObjectResult("Please pass a 'userId' on the query string or in the request body");
        }
    }
}
